package jc.scratch

import org.junit.Before
import org.junit.Test

import static jc.scratch.Application.im
import static org.junit.Assert.assertEquals as eq

class ApplicationTest {
    def app

    @Before
    void setup() {
        app = new Application()
    }

    @Test
    void testEcho() {
        eq("echo: test", app.echo("test"))
    }

    @Test(expected = UnsupportedOperationException)
    void testImmutableMap() {
        def map = [a: 1, b: false]
        map.c = "test"
        def imMap = im(map)
        println imMap
        imMap.b = true
    }

    @Test(expected = UnsupportedOperationException)
    void testImmutableList() {
        def list = [1, 2, 3]
        list << 4
        def imList = im(list)
        println imList
        imList << 5
    }

}
