package jc.scratch

class Application {

    static Collection im(Collection collection) {
        collection.asImmutable()
    }

    static Map im(Map map) {
        map.asImmutable()
    }

    String echo(String message) {
        "echo: $message"
    }

    static void main(String... args) {
        println("yo groovy!")
    }

}